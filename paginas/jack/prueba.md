Title: Cómo usar jack
Date: 2019-03-10 9:00
Category: wiki
Tags: jack, linux, sonido, audio
Author: rexmalebka

### ¿Qué es JACK?

Jack es un servidor de audio muy util, su finalidad es básicamente la de transmitir informaión a los drivers y tarjetas de sonido

### ALSA

ALSA (Advanced Linux Sound Architecture) Arquitectura de Sonido Avanzada para Linux, ALSA es la parte del kernel Linux que brinda soporte para audio y MIDI, brinda una API para implementar aplicaciones que soporten audio y/o MIDI e implementa un conjunto de drivers (controladores). La aplicación alsa-mixer (terminal) y control de volumen son muy útiles para establecer los niveles y parámetros de las tarjetas de sonido.

Por otro lado están los controladores FFADO para tarjetas de sonido FireWire. Análogo a alsa-mixer o control de volumen, está FFADO Mixer.

En muchos casos y dependiendo del hardware, ALSA solo soporta que una aplicación utilice la tarjeta de sonido simultáneamente. Para solucionar esto y para brindar una experiencia transparente a los usuarios, se creó Jack y PulseAudio. 

### PulseAudio

Es un proxy entre las aplicaciones de usuario y ALSA. Es decir, es quien permite que aplicaciones como Firefox, el plugin de Flash, Skype, etc. utilicen los recursos brindados por ALSA. Normalmente no hay que preocuparse por la existencia de PulseAudio, excepto cuando se quiere lograr que aplicaciones de Jack y aplicaciones de PulseAudio funcionen simultáneamente.

ALSA, FFADO y PulseAudio vienen instaladas por defecto en la mayoría de las distribuciones de GNU-Linux.


JACK Status
- Server Status le indica si el servidor JACK está funcionando o no. No se puede ejecutar ningún software JACK dependiente a menos que el servidor JACK se inicie con anterioridad.
- Realtime le indica si JACK se está ejecutando con la programación en tiempo real o no. programación en tiempo real permite a Jack tener prioridad sobre otras aplicaciones, mejorando el rendimiento de las aplicaciones JACK.
- DSP Load es la carga de la CPU actual estimada por JACK. Si se pone demasiado alta puede experimentar problemas de rendimiento.
- Xruns son interrupciones o cortes en el audio que ocurren cuando el buffer de la memoria no proporciona un flujo constante de datos a una aplicación Jack. La clave del éxito con JACK es eliminar o evitar xruns.
- Buffer Size Determina la latencia (retardo) entre el audio que está siendo recibido por Jack y el que se envía al dispositivo de salida. los tamaños de búfer más po
tienen menos latencia. 1024 es el valor predeterminado y es por lo general un valor seguro, pero pruebe valores diferentes para grabar o reproducir sin generar Xruns. El funcionamiento de los Buffer de menor tamaño depende de la potencia de la CPU y los detalles de su dispositivo de audio.
- Sample rate es la frecuencia de muestreo, JACK reproduce y grabar audio en 44,1 kHz estándar de un CD de audio, 48kHz para trabajar con vídeo u otro tipo de audio. frecuencias de muestreo más altas permiten una mayor calidad, pero ocupan más espacio en disco. Incluso para fines profesionales.
- Latency es el retraso de captura en JACK, mide el tiempo necesario para llenar el búffer de captura de audio. Este valor se determina con los ajustes en buffer Size y sample rate utilizados al iniciar JACK.

JACK Controls

- Start Inicia el servidor JACK, esta es la forma recomendada para iniciar JACK.
- Stop detiene el servidor JACK. Por seguridad es recomendable guardar y cerrar las aplicaciones que usen Jack.
- Force restart obliga el reinicio del servidor JACK, pero es posible que pierda los últimos cambios realizados en la configuración de JACK.
- Configure abre la ventana de configuración de JACK.
- Switch Master le dice a Jack que aplique todos los cambios realizados en la ventana de configuración de JACK.
- Auto-start JACK or LADISH at login te permite elegir: si desea iniciar JACK o un estudio LADISH. Cadence incluye una aplicación llamada Claudia que permite crear escenarios virtuales de aplicaciones de audio conectadas entre sí llamados estudio. Después de crear un estudio dentro de Claudia, puede utilizar CADENCE para configurar el arranque automático de este con todos los ajustes guardados.



![alt text]({attach}audio.png "jack")
